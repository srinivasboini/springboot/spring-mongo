package com.srini.mongo;

import com.srini.mongo.collections.Book;
import com.srini.mongo.repo.AppRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.UUID;

/**
 * The type Spring mongo application.
 */
@SpringBootApplication
@RequiredArgsConstructor
public class SpringMongoApplication {

	private final AppRepository appRepository ;

	/**
	 * The entry point of application.
	 *
	 * @param args the input arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(SpringMongoApplication.class, args);
	}


	/**
	 * Execute.
	 */
	@EventListener(ApplicationStartedEvent.class)
	public void execute(){
		Book book = new Book(UUID.randomUUID().toString(),"test","test","test") ;
		//mongoTemplate.createCollection("book") ;
		//mongoTemplate.save(book) ;
		appRepository.insert(book) ;
	}

}
