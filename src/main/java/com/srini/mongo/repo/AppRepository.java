package com.srini.mongo.repo;

import com.srini.mongo.collections.Book;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * The interface App repository.
 */
@Repository
public interface AppRepository extends MongoRepository<Book, String> {
}
